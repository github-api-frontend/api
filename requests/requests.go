package requests

import (
	"encoding/json"
	"gitlab.com/github-api-frontend/api/app/context"
	"log"
	"net/http"
)

type Response struct {
	Status int `json:"-"`
	Message string `json:"message,omitempty"`
}

var (
	ResponseOK = &Response{Status: 200}
	ResponseNotFound = &Response{Status: 404}
	ResponseUnauthorized = &Response{Status: 401, Message: "unauthorized"}
	ResponseInternalServerError = &Response{Status: 500, Message: "internal server error"}
	ResponseBadRequest = &Response{Status: 400, Message: "bad request"}
)

// RequestHandler is used to give HTTP handlers access to the app context
type RequestHandler struct {
	Context *context.Context
	Handler func(*context.Context, http.ResponseWriter, *http.Request) (interface{}, *Response, error)
}

// ServeHTTP passes the app context into request handlers and writes their response as JSON
func (requestHandler RequestHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	responseStruct, response, err := requestHandler.Handler(
		requestHandler.Context, w, r)

	if err != nil {
		log.Println("ERROR:", err.Error())
		w.WriteHeader(ResponseInternalServerError.Status)
		json.NewEncoder(w).Encode(ResponseInternalServerError)
		return
	}

	if response.Status != 200 {
		w.WriteHeader(response.Status)
		json.NewEncoder(w).Encode(response)
		return
	}

	w.WriteHeader(response.Status)
	json.NewEncoder(w).Encode(responseStruct)
}