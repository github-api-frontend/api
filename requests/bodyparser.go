package requests

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// ParseBody parses the body of a request into v and returns whether or not the input was valid
func ParseBody(r *http.Request, v interface{}) error {
	bodyContent, err := ioutil.ReadAll(r.Body)
	if err != nil { return err }

	return json.Unmarshal(bodyContent, v)
}