package database

import (
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const (
	CollectionUsers = "users"
)

type UserData struct {
	Favorites []Favorite `bson:"favorites"`
}

type UserInfo struct {
	UserId int `bson:"userid"`
	Username       string `bson:"username"`
	PasswordHashed string `bson:"password"`
	LoginOrigin string `bson:"login_origin"`
	Administrator bool `bson:"administrator"`
	Data UserData `json:"user_data"`
}

// NewUserData returns a new instance of UserData
func NewUserData() UserData {
	return UserData{Favorites: []Favorite{}}
}

// InsertUser inserts a user with the given info
func (d *DB) InsertUser(info UserInfo) (primitive.ObjectID, error) {
	var objectId primitive.ObjectID

	userExists, err := d.UserExists(info.Username)
	if err != nil {
		return objectId, err
	}

	if userExists {
		return objectId, errors.New(fmt.Sprintf("user %s already exists", info.Username))
	}

	info.Data = NewUserData()

	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	res, err := collection.InsertOne(d.ctx, info)

	objectId = res.InsertedID.(primitive.ObjectID)

	return objectId, err
}

// DeleteUser deletes all users with the given name
func (d *DB) DeleteUser(username string) error {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	_, err := collection.DeleteMany(d.ctx, bson.M{"username": username})

	return err
}

// DeleteUserById deletes all users with the given name
func (d *DB) DeleteUserById(id int) error {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	_, err := collection.DeleteMany(d.ctx, bson.M{"userid": id})

	return err
}

// GetUser gets all information about a user with the given name
func (d *DB) GetUser(username string) (UserInfo, error) {
	var output UserInfo
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	err := collection.FindOne(d.ctx, bson.M{"username": username}).Decode(&output)

	return output, err
}

// GetUserById gets all information about a user with the given name
func (d *DB) GetUserById(id int) (UserInfo, error) {
	var output UserInfo
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	err := collection.FindOne(d.ctx, bson.M{"userid": id}).Decode(&output)

	return output, err
}

// UserExists returns whether or not a user exists in the database
func (d *DB) UserExists(username string) (bool, error) {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	count, err := collection.CountDocuments(d.ctx, bson.M{"username": username})
	if err != nil {
		return false, err
	}

	return count >= 1, nil
}

// UserExistsById returns whether or not a user exists in the database
func (d *DB) UserExistsById(id int) (bool, error) {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	count, err := collection.CountDocuments(d.ctx, bson.M{"userid": id})
	if err != nil {
		return false, err
	}

	return count >= 1, nil
}

// UserUpdateOrigin updates the user's last login origin
func (d *DB) UserUpdateOrigin(userid int, origin string) error {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	_, err := collection.UpdateOne(d.ctx,
		bson.M{"userid": userid},
		bson.M{"$set": bson.M{"login_origin": origin}})

	return err
}

// UserUpdateAdministrator updates the user's administrative role
func (d *DB) UserUpdateAdministrator(userid int, status bool) error {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	_, err := collection.UpdateOne(d.ctx,
		bson.M{"userid": userid},
		bson.M{"$set": bson.M{"administrator": status}})

	return err
}

// UserIsAdministrator returns whether or not a user is an administrator
func (d *DB) UserIsAdministrator(userid int) (bool, error) {
	userInfo, err := d.GetUserById(userid)
	if err != nil {
		return false, err
	}

	return userInfo.Administrator, nil
}

// GetAllUsers gets all registered users
func (d *DB) GetAllUsers() ([]UserInfo, error) {
	var users []UserInfo

	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	cursor, err := collection.Find(d.ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	if err := cursor.All(d.ctx, &users); err != nil {
		return nil, err
	}

	return users, err
}