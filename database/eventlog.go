package database

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

const CollectionEventLog = "event_log"

// Event stores all information about an event
type Event struct {
	Timestamp primitive.DateTime `bson:"timestamp" json:"timestamp"`
	UserId int `bson:"userid" json:"userid"`
	IP string `bson:"ip" json:"ip"`
	Action string `bson:"action" json:"action"`
	Details string `bson:"details" json:"details"`
}

// AddEvent adds the given event to the event log
func (d *DB) AddEvent(event Event) (primitive.ObjectID, error) {
	collection := d.mongo.Database(DbName).Collection(CollectionEventLog)
	res, err := collection.InsertOne(d.ctx, event)

	return res.InsertedID.(primitive.ObjectID), err
}

// GetEvent gets the event with the given ID from the event log
func (d *DB) GetEvent(id primitive.ObjectID) (Event, error) {
	var event Event

	collection := d.mongo.Database(DbName).Collection(CollectionEventLog)
	err := collection.FindOne(d.ctx, bson.M{"_id": id}).Decode(&event)

	return event, err
}

// RemoveEvent removes the event with the given ID from the event log
func (d *DB) RemoveEvent(id primitive.ObjectID) error {
	collection := d.mongo.Database(DbName).Collection(CollectionEventLog)
	_, err := collection.DeleteMany(d.ctx, bson.M{"_id": id})

	return err
}

// GetAllEvents gets all internal events
func (d *DB) GetAllEvents() ([]Event, error) {
	var events []Event

	collection := d.mongo.Database(DbName).Collection(CollectionEventLog)
	cursor, err := collection.Find(d.ctx, bson.M{})
	if err != nil {
		return nil, err
	}

	if err := cursor.All(d.ctx, &events); err != nil {
		return nil, err
	}

	return events, err
}