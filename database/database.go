package database

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const DbName = "gafapi"

// DB is the primary database handler and provides app-specific functions
type DB struct {
	mongo *mongo.Client
	ctx context.Context
}

// NewDB returns a pointer to a DB instance initialized with the given connection string
func NewDB(host string) (*DB, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI(host))
	if err != nil {
		return nil, err
	}

	return &DB{
		mongo: client,
	}, nil
}

// Connect connects to the provided mongodb server
func (d *DB) Connect() error {
	ctx := context.Background()
	if err := d.mongo.Connect(ctx); err != nil {
		return err
	}

	if err := d.mongo.Ping(ctx, readpref.Primary()); err != nil {
		return err
	}

	d.ctx = ctx

	return nil
}

// Disconnect disconnects from the provided mongodb server
func (d *DB) Disconnect() error {
	if err := d.mongo.Disconnect(d.ctx); err != nil {
		return err
	}

	return nil
}