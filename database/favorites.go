package database

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson"
)

type Favorite = int64

type FavoritesInfo struct {
	Favorites []Favorite `bson:"favorites"`
}

// GetFavorites gets a list of favorite ids associated with a user
func (d *DB) GetFavorites(userId int) (FavoritesInfo, error) {
	var output FavoritesInfo

	userInfo, err := d.GetUserById(userId)
	if err != nil {
		return output, err
	}

	return FavoritesInfo{
		Favorites: userInfo.Data.Favorites,
	}, err
}

func (d *DB) IsFavorited(userId int, favorite Favorite) (bool, error) {
	favorites, err := d.GetFavorites(userId)
	if err != nil {
		return false, err
	}

	for _, v := range favorites.Favorites {
		if v == favorite {
			return true, nil
		}
	}

	return false, nil
}

// AddFavorite appends the given favorite to the given user's favorites
func (d *DB) AddFavorite(userId int, favorite Favorite) error {
	isFavorited, err := d.IsFavorited(userId, favorite)
	if err != nil {
		return err
	}

	if isFavorited {
		return errors.New("item is already favorited")
	}

	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	change := bson.M{
		"$push": bson.M{"data.favorites": favorite},
	}

	_, err = collection.UpdateOne(d.ctx, bson.M{"userid": userId}, change)
	return err
}

// RemoveFavorite removes the given favorite from the user's favorites
func (d *DB) RemoveFavorite(userId int, favorite Favorite) error {
	collection := d.mongo.Database(DbName).Collection(CollectionUsers)
	change := bson.M{
		"$pull": bson.M{"data.favorites": favorite},
	}

	_, err := collection.UpdateOne(d.ctx, bson.M{"userid": userId}, change)
	return err
}