package auth

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/github-api-frontend/api/shared"
	"net/http"
	"strconv"
	"time"
)

// JWTMetadata stores all JWT user data
type JWTMetadata struct {
	UserId int
}

// CreateJWTToken generates a JWT token to be sent to a client
func CreateJWTToken(userid int) (string, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["userid"] = userid
	claims["exp"] = time.Now().Add(time.Hour*24).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := at.SignedString([]byte(shared.EnvJWTSigningKey))
	if err != nil {
		return "", err
	}

	return token, nil
}

// GetJWTMetadataFromString validates a JWT token and returns its metadata
func GetJWTMetadataFromString(token string) (*JWTMetadata, error) {
	jwtValidator := func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid token signing method")
		}

		return []byte(shared.EnvJWTSigningKey), nil
	}

	tokenParsed, err := jwt.Parse(token, jwtValidator)
	if err != nil {
		return nil, err
	}

	return GetJWTMetadata(tokenParsed)
}

// GetJWTMetadata gets metadata about a JWT token
func GetJWTMetadata(token *jwt.Token) (*JWTMetadata, error) {
	if _, ok := token.Claims.(jwt.Claims); !ok {
		return nil, errors.New("invalid token claims")
	}

	if !token.Valid {
		return nil, errors.New("invalid token")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("missing claims from token")
	}

	userid, err := strconv.ParseUint(fmt.Sprintf("%.f", claims["userid"]),10, 64)
	if err != nil {
		return nil, err
	}

	return &JWTMetadata{
		UserId: int(userid),
	}, nil
}

// SetResponseJWTCookie sets a response cookie for the given JWT token
func SetResponseJWTCookie(w http.ResponseWriter, token string) {
	http.SetCookie(w, &http.Cookie{
		Name: "jwt",
		Value: token,
		HttpOnly: true,
		Secure: true,
	})
}