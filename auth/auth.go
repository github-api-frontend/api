package auth

import (
	"errors"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/database"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"time"
)

type UserInfo struct {
	Username string
	Password string
}

// RegisterUser hashes the given password and inserts a new User record into the database
// If the registration is successful, the new userid is returned
func RegisterUser(ctx *context.Context, info UserInfo) (int, error) {
	randInst := rand.New(rand.NewSource(time.Now().UnixNano()))
	tryGenerateId := func() (int, bool, error) {
		id := randInst.Intn(2147483647)

		userExists, err := ctx.Database.UserExistsById(id)
		if err != nil {
			return -1, false, err
		}

		return id, !userExists, nil
	}

	passHashed, err := bcrypt.GenerateFromPassword([]byte(info.Password), bcrypt.DefaultCost)
	if err != nil {
		return -1, err
	}

	var id int

	for {
		localId, isUnused, err := tryGenerateId()
		if err != nil {
			return -1, err
		}

		if isUnused {
			id = localId
			break
		}
	}

	_, err = ctx.Database.InsertUser(database.UserInfo{
		UserId: id,
		Username:       info.Username,
		PasswordHashed: string(passHashed),
		Administrator: false,
	})

	return id, err
}

// CheckValidUser returns whether the given login credentials are valid
func CheckValidUser(ctx *context.Context, username string, password string) (bool, error) {
	exists, err := ctx.Database.UserExists(username)
	if err != nil {
		return false, err
	}

	if !exists {
		return false, errors.New("username does not exist")
	}

	user, err := ctx.Database.GetUser(username)
	if err != nil {
		return false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHashed), []byte(password))

	return err == nil, nil
}