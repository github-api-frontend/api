build:
	go build -o bin/github-api-frontend ./cmd

test:
	go clean -testcache
	go test -v ./...

clean:
	rm -rf bin