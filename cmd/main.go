package main

import (
	"gitlab.com/github-api-frontend/api/app"
	"gitlab.com/github-api-frontend/api/shared"
	"log"
	"strings"
)

func main() {
	service, err := app.NewService()
	if err != nil {
		log.Fatal(err)
	}

	service.Initialize()

	if strings.ToLower(shared.EnvUseHTTPS) == "true" {
		err = service.ListenAndServeTLS(shared.EnvAPIHostAddress, shared.EnvCertFile, shared.EnvKeyFile)
	} else {
		err = service.ListenAndServe(shared.EnvAPIHostAddress)
	}

	if err != nil {
		log.Fatal(err)
	}
}
