package app

import (
	"github.com/asaskevich/govalidator"
	"gitlab.com/github-api-frontend/api/app/context"
	"log"
)

// Service stores the global state of the app
type Service struct {
	Context *context.Context
	Router  *Router
}

// NewService creates a new instance of Service and returns its pointer
func NewService() (*Service, error) {
	service := Service{}

	ctx, err := context.NewContext()
	if err != nil {
		return nil, err
	}

	service.Context = ctx

	service.Router = NewRouter(service.Context)

	return &service, nil
}

// Initialize initializes all aspects of the app
func (service *Service) Initialize() error {
	govalidator.SetFieldsRequiredByDefault(true)

	return service.Router.InitializeRoutes()
}

// ListenAndServe creates an HTTP server and listens on the given address
func (service *Service) ListenAndServe(address string) error {
	log.Println("[Service] service.ListenAndServe()")
	return service.Router.ListenAndServe(address)
}

// ListenAndServeTLS creates an HTTPS server and listens on the given address
func (service *Service) ListenAndServeTLS(address string, cert string, key string) error {
	log.Println("[Service] service.ListenAndServeTLS()")
	return service.Router.ListenAndServeTLS(address, cert, key)
}