package context

import (
	"context"
	"github.com/google/go-github/github"
	"github.com/gregjones/httpcache"
	redisCache "github.com/gregjones/httpcache/redis"
	"gitlab.com/github-api-frontend/api/database"
	redis "gitlab.com/github-api-frontend/api/redis"
	"gitlab.com/github-api-frontend/api/shared"
	"golang.org/x/oauth2"
	"log"
	"net/http"
)

// Context stores the current app context, including database and cache connections
type Context struct {
	Github *github.Client
	GithubContext context.Context
	RedisPool *redis.Pool
	Database *database.DB
}

// NewContext returns a pointer to a new Context instance
func NewContext() (*Context, error) {
	var ctx Context

	db, err := database.NewDB(shared.EnvMongoConnection)
	if err != nil {
		return nil, err
	}

	if err := db.Connect(); err != nil {
		return nil, err
	}

	ctx.Database = db

	ctx.RedisPool = redis.NewPool(shared.EnvRedisHost)

	cacheConn := ctx.RedisPool.Get()

	ctx.GithubContext = context.Background()

	if len(shared.EnvGithubToken) > 0 {
		ts := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: shared.EnvGithubToken})

		tc := &oauth2.Transport{
			Source: ts,
		}

		gitTransport := &httpcache.Transport{
			Transport: tc,
			Cache: redisCache.NewWithClient(cacheConn.Conn()),
		}

		log.Println("[Context] Creating Github instance with auth...")
		ctx.Github = github.NewClient(&http.Client{
			Transport:     gitTransport,
		})
	} else {
		gitTransport := httpcache.NewTransport(redisCache.NewWithClient(
			cacheConn.Conn()))

		log.Println("[Context] Creating Github instance without auth...")
		ctx.Github = github.NewClient(&http.Client{
			Transport: gitTransport,
		})
	}

	return &ctx, nil
}