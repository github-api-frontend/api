package app

import (
	"encoding/json"
	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/requests"
	"gitlab.com/github-api-frontend/api/shared"
	"net/http"
	"os"
)

// mergeParamsMiddleware merges all request params (i.e. /repo/{repo}/info) into the request form for parsing
func mergeParamsMiddleware(next http.Handler) http.Handler {
	h := func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		for i, v := range mux.Vars(r) {
			r.Form.Add(i, v)
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(h)
}

// loggingMiddleware logs all requests in the Apache Common Log Format
func loggingMiddleware(next http.Handler) http.Handler {
	return handlers.LoggingHandler(os.Stdout, next)
}

// jwtValidation validates that a JWT token is signed and valid
var jwtValidationMiddleware = jwtmiddleware.New(jwtmiddleware.Options{
	ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
		return []byte(shared.EnvJWTSigningKey), nil
	},
	ErrorHandler: func(w http.ResponseWriter, r *http.Request, err string) {
		w.WriteHeader(400)
		json.NewEncoder(w).Encode(requests.Response{
			Message: err,
		})
	},
	Extractor: jwtmiddleware.FromFirst(jwtmiddleware.FromAuthHeader, func(r *http.Request) (string, error) {
		jwtCookie, err := r.Cookie("jwt")
		if err != nil {
			return "", err
		}

		return jwtCookie.Value, nil
	}),
	SigningMethod: jwt.SigningMethodHS256,
}).Handler

// administrationMiddleware ensures that all requests to an endpoint are made by an administrator
func (router *Router) administrationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := r.Context().Value("user").(*jwt.Token)

		jwtMetadata, err := auth.GetJWTMetadata(user)
		if err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(requests.ResponseInternalServerError)
			return
		}

		status, err := router.context.Database.UserIsAdministrator(jwtMetadata.UserId)
		if err != nil {
			w.WriteHeader(500)
			json.NewEncoder(w).Encode(requests.ResponseInternalServerError)
			return
		}

		if !status {
			w.WriteHeader(401)
			json.NewEncoder(w).Encode(requests.Response{Status: 401, Message: "unauthorized"})
			return
		}

		next.ServeHTTP(w, r)
	})
}

// accessOriginMiddleware attaches an origin policy to the header of the response
func accessOriginMiddleware(next http.Handler) http.Handler {
	return cors.New(cors.Options{
		AllowedOrigins: shared.EnvOriginPolicy,
		AllowCredentials: true,
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
	}).Handler(next)
}