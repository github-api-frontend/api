package app

import (
	"github.com/gorilla/mux"
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/requests"
	"log"
	"net/http"
)

// Router handles routing REST API requests and passing the app Context down
type Router struct {
	router *mux.Router
	context *context.Context
}

// NewRouter returns a pointer a Router with the provided app Context
func NewRouter(context *context.Context) *Router {
	return &Router{
		router: mux.NewRouter(),
		context: context,
	}
}

// Mux returns a reference to the internal mux.Router
func (router *Router) Mux() *mux.Router {
	return router.router
}

// InitializeRoutes initializes all Router routes
func (router *Router) InitializeRoutes() error {
	log.Println("[Router] initializing routes...")

	// Initialize app routes
	router.router.Use(accessOriginMiddleware)
	router.router.Use(mergeParamsMiddleware)
	router.router.Use(loggingMiddleware)

	router.setupRootRoutes()
	router.setupUserRoutes()
	router.setupRepoRoutes()
	router.setupAdminRoutes()

	return nil
}

func (router *Router) setupRootRoutes() {
	router.router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}).Methods("OPTIONS")

	router.router.Handle("/login", requests.RequestHandler{
		router.context,
		api.LoginPOST,
	}).Methods("POST", "OPTIONS")

	router.router.Handle("/logout", requests.RequestHandler{
		router.context,
		api.LogoutPOST,
	}).Methods("POST", "OPTIONS")

	router.router.Handle("/register", requests.RequestHandler{
		router.context,
		api.RegisterPOST,
	}).Methods("POST", "OPTIONS")
}

func (router *Router) setupUserRoutes() {
	userRoute := router.router.PathPrefix("/user").Subrouter()

	// Setup middleware for user authentication and Context injection
	userRoute.Use(jwtValidationMiddleware)

	userRoute.Handle("/info", requests.RequestHandler{
		Context: router.context,
		Handler: api.UserInfoGET,
	}).Methods("GET", "OPTIONS")

	userRoute.Handle("/favorites", requests.RequestHandler{
		Context: router.context,
		Handler: api.UserFavoritesGET,
	}).Methods("GET", "OPTIONS")

	userRoute.Handle("/favorite/{repo}", requests.RequestHandler{
		Context: router.context,
		Handler: api.UserFavoriteGET,
	}).Methods("GET", "OPTIONS")

	userRoute.Handle("/favorites", requests.RequestHandler{
		Context: router.context,
		Handler: api.UserFavoritesPUT,
	}).Methods("PUT", "OPTIONS")

	userRoute.Handle("/favorites", requests.RequestHandler{
		Context: router.context,
		Handler: api.UserFavoritesDELETE,
	}).Methods("DELETE", "OPTIONS")
}

func (router *Router) setupRepoRoutes() {
	repoRoute := router.router.PathPrefix("/repo").Subrouter()

	repoRoute.Handle("/search", requests.RequestHandler{
		Context: router.context,
		Handler: api.RepoSearchPOST,
	}).Methods("POST", "OPTIONS")

	repoRoute.Handle("/{repo}/info", requests.RequestHandler{
		Context: router.context,
		Handler: api.RepoInfoGET,
	}).Methods("GET", "OPTIONS")
}

func (router *Router) setupAdminRoutes() {
	adminRoute := router.router.PathPrefix("/admin").Subrouter()
	adminRoute.Use(jwtValidationMiddleware)
	adminRoute.Use(router.administrationMiddleware)

	adminRoute.Handle("/events", requests.RequestHandler{
		Context: router.context,
		Handler: api.AdminEventsGET,
	}).Methods("GET", "OPTIONS")

	adminRoute.Handle("/users", requests.RequestHandler{
		Context: router.context,
		Handler: api.AdminUsersGET,
	}).Methods("GET", "OPTIONS")
}

// ListenAndServe creates an HTTP server and listens on the given address
func (router *Router) ListenAndServe(address string) error {
	return http.ListenAndServe(address, router.router)
}

func (router *Router) ListenAndServeTLS(address string, cert string, key string) error {
	return http.ListenAndServeTLS(address, cert, key, router.router)
}
