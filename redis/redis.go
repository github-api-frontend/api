package redis

import "github.com/gomodule/redigo/redis"

// Pool wraps a *redis.Pool with app specific functions
type Pool struct {
	pool *redis.Pool
}

// Connection wraps a redis.Conn with app specific functions
type Connection struct {
	conn redis.Conn
}

// NewPool returns a new pool configured with the given host
func NewPool(host string) *Pool {
	var pool Pool

	pool.pool = &redis.Pool{
		MaxIdle: 80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			conn, err := redis.Dial("tcp", host)
			if err != nil {
				return nil, err
			}

			return conn, nil
		},
	}

	return &pool
}

// Get gets a new connection from the redis pool
func (p *Pool) Get() *Connection {
	conn := p.pool.Get()

	return &Connection{conn: conn}
}

// Close closes the pool and all existing connections
func (p *Pool) Close() error {
	if err := p.pool.Close(); err != nil {
		return err
	}

	return nil
}

// Conn gets a pointer to the redis connection
func (c *Connection) Conn() redis.Conn {
	return c.conn
}

// Close closes the connection
func (c *Connection) Close() error {
	if err := c.conn.Close(); err != nil {
		return err
	}

	return nil
}