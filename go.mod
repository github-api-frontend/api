module gitlab.com/github-api-frontend/api

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef
	github.com/auth0/go-jwt-middleware v0.0.0-20201030150249-d783b5c46b39
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gomodule/redigo v1.8.3
	github.com/google/go-github v17.0.0+incompatible
	github.com/google/go-github/v29 v29.0.3 // indirect
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79
	github.com/rs/cors v1.7.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.4.4
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/oauth2 v0.0.0-20201208152858-08078c50e5b5
)
