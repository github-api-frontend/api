FROM golang:1.15.6-alpine AS builder

RUN apk add git make

WORKDIR /app

COPY go.mod .

RUN go mod download

COPY . .

RUN make build



FROM alpine:3.12.3

WORKDIR /app

COPY --from=builder /app/bin/github-api-frontend .

CMD cd /app && ./github-api-frontend