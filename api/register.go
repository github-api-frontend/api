package api

import (
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"gitlab.com/github-api-frontend/api/requests"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"strings"
	"time"
)

type RegisterPOSTRequest struct {
	Username string `json:"username" valid:"ascii,length(3|24),required"`
	Password string `json:"password" valid:"ascii,length(8|24),required"`
}

type RegisterPOSTResponse struct {
	Token string `json:"token"`
}

// RegisterPOST authenticates a user and returns a JWT token
func RegisterPOST(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	var requestData RegisterPOSTRequest
	if response, err := parseAndValidateBody(r, &requestData); err != nil {
		return nil, response, err
	}

	requestData.Username = strings.ToLower(requestData.Username)

	userExists, err := ctx.Database.UserExists(requestData.Username)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	if userExists {
		return nil, &requests.Response{Status: 400, Message: "user already exists"}, nil
	}

	id, err := auth.RegisterUser(ctx, auth.UserInfo{
		Username: requestData.Username,
		Password: requestData.Password,
	})
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	ctx.Database.UserUpdateOrigin(id, r.RemoteAddr)

	token, err := auth.CreateJWTToken(id)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	_, err = ctx.Database.AddEvent(database.Event{
		Timestamp: primitive.NewDateTimeFromTime(time.Now()),
		UserId:    id,
		IP:        r.RemoteAddr,
		Action:    "REGISTER",
		Details:   "username: " + requestData.Username,
	})

	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	auth.SetResponseJWTCookie(w, token)

	return RegisterPOSTResponse{Token: token}, requests.ResponseOK, nil
}