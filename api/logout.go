package api

import (
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

// LogoutPOST deletes the JWT cookie from the browser session
func LogoutPOST(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	auth.SetResponseJWTCookie(w, "")

	return nil, requests.ResponseOK, nil
}

