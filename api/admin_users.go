package api

import (
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

type AdminUsersGETUser struct {
	UserId int `bson:"userid"`
	Username       string `bson:"username"`
	LoginOrigin string `bson:"login_origin"`
	Administrator bool `bson:"administrator"`
}

type AdminUsersGETResponse struct {
	Users []AdminUsersGETUser `json:"users"`
}

// AdminUsersGET gets information about every user
func AdminUsersGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	var response AdminUsersGETResponse

	users, err := ctx.Database.GetAllUsers()
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	for _,user := range users {
		response.Users = append(response.Users, AdminUsersGETUser{
			UserId:        user.UserId,
			Username:      user.Username,
			LoginOrigin:   user.LoginOrigin,
			Administrator: user.Administrator,
		})
	}

	return response, requests.ResponseOK, nil
}

