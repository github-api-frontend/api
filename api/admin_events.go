package api

import (
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/database"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

type AdminEventsGETResponse struct {
	Events []database.Event `json:"events"`
}

// AdminEventsGET gets the application event logs
func AdminEventsGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	events, err := ctx.Database.GetAllEvents()
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	return AdminEventsGETResponse{
		Events: events,
	}, requests.ResponseOK, nil
}
