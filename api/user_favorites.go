package api

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"gitlab.com/github-api-frontend/api/requests"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"strconv"
	"time"
)

type UserFavoritesGETResponse struct {
	Favorites []database.Favorite `json:"favorites"`
}

type UserFavoriteGETRequestParams struct {
	RepoId database.Favorite `schema:"repo,required" valid:"int,required"`
}

type UserFavoriteGETResponse struct {
	Favorited bool `json:"favorited"`
}

type UserFavoritesPUTRequest struct {
	Favorite database.Favorite `json:"favorite" valid:"int,required"`
}

type UserFavoritesDELETERequest struct {
	Favorite database.Favorite `json:"favorite" valid:"int,required"`
}

// UserFavoritesGET gets all of the current user's favorites
func UserFavoritesGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	user := r.Context().Value("user").(*jwt.Token)

	jwtMetadata, err := auth.GetJWTMetadata(user)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	favorites, err := ctx.Database.GetFavorites(jwtMetadata.UserId)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	return UserFavoritesGETResponse{Favorites: favorites.Favorites}, requests.ResponseOK, nil
}

// UserFavoriteGET gets whether or not the user have favorited a repo
func UserFavoriteGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	user := r.Context().Value("user").(*jwt.Token)

	// Get JWT metadata
	jwtMetadata, err := auth.GetJWTMetadata(user)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	// Get request info
	var requestParams UserFavoriteGETRequestParams

	if response, err := decodeAndValidateSchema(r, &requestParams); err != nil {
		return nil, response, err
	}

	// Get favorites
	favorites, err := ctx.Database.GetFavorites(jwtMetadata.UserId)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	// Check whether favorite exists
	favorited := false

	for _, favorite := range favorites.Favorites {
		if favorite == requestParams.RepoId {
			favorited = true
		}
	}

	return UserFavoriteGETResponse{Favorited: favorited}, requests.ResponseOK, nil
}

// UserFavoritesPUT adds a favorite to the user's favorites
func UserFavoritesPUT(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	user := r.Context().Value("user").(*jwt.Token)

	jwtMetadata, err := auth.GetJWTMetadata(user)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	var requestData UserFavoritesPUTRequest
	if response, err := parseAndValidateBody(r, &requestData); err != nil {
		return nil, response, err
	}

	err = ctx.Database.AddFavorite(jwtMetadata.UserId, requestData.Favorite)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	_, err = ctx.Database.AddEvent(database.Event{
		Timestamp: primitive.NewDateTimeFromTime(time.Now()),
		UserId:    jwtMetadata.UserId,
		IP:        r.RemoteAddr,
		Action:    "FAVORITE",
		Details:   "repo: " + strconv.Itoa(int(requestData.Favorite)),
	})

	return nil, &requests.Response{Status: 200, Message: "successfully added favorite"}, nil
}

// UserFavoritesDELETE adds a favorite to the user's favorites
func UserFavoritesDELETE(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	user := r.Context().Value("user").(*jwt.Token)

	jwtMetadata, err := auth.GetJWTMetadata(user)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	var requestData UserFavoritesDELETERequest
	if response, err := parseAndValidateBody(r, &requestData); err != nil {
		return nil, response, err
	}

	err = ctx.Database.RemoveFavorite(jwtMetadata.UserId, requestData.Favorite)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	_, err = ctx.Database.AddEvent(database.Event{
		Timestamp: primitive.NewDateTimeFromTime(time.Now()),
		UserId:    jwtMetadata.UserId,
		IP:        r.RemoteAddr,
		Action:    "UNFAVORITE",
		Details:   "repo: " + strconv.Itoa(int(requestData.Favorite)),
	})

	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	return nil, &requests.Response{Status: 200, Message: "successfully removed favorite"}, nil
}
