package api

import (
	"github.com/asaskevich/govalidator"
	"github.com/gorilla/schema"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

// schemaDecoder is stored as a static for caching
var schemaDecoder *schema.Decoder = schema.NewDecoder()

// parseAndValidateBody decodes the given request into the given struct and returns an error/response
func parseAndValidateBody(r *http.Request, v interface{}) (*requests.Response, error) {
	if err := requests.ParseBody(r, v); err != nil {
		return requests.ResponseBadRequest, err
	}

	valid, err := govalidator.ValidateStruct(v)
	if !valid {
		return &requests.Response{400, err.Error()}, nil
	}

	return nil, nil
}

// decodeAndValidateSchema decodes the given request into the given schema and returns an error/response
func decodeAndValidateSchema(r *http.Request, v interface{}) (*requests.Response, error) {
	if err := r.ParseForm(); err != nil {
		return requests.ResponseInternalServerError, err
	}

	if err := schemaDecoder.Decode(v, r.Form); err != nil {
		return &requests.Response{400, "bad request params"}, err
	}

	valid, err := govalidator.ValidateStruct(v)
	if !valid {
		return &requests.Response{400, err.Error()}, nil
	}

	return nil, nil
}