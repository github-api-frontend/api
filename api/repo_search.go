package api

import (
	"github.com/google/go-github/github"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

type RepoSearchPOSTRequest struct {
	Query string `json:"query" valid:"type(string),required"`
	Page int `json:"page" valid:"-"`
	PerPage int `json:"per_page" valid:"-"`
}

type RepoSearchGETResponse struct {
	TotalRepos int `json:"total"`
	Repos []github.Repository `json:"repos"`
}

// RepoSearchPOST gets all of the repositories matching the given query
func RepoSearchPOST(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	var requestData RepoSearchPOSTRequest
	if response, err := parseAndValidateBody(r, &requestData); err != nil {
		return nil, response, err
	}

	result, _, err := ctx.Github.Search.Repositories(ctx.GithubContext, requestData.Query, &github.SearchOptions{
		ListOptions: github.ListOptions{
			Page: requestData.Page,
			PerPage: requestData.PerPage,
		},
	})

	if err != nil {
		return nil,& requests.Response{Status: 500, Message: "unable to communicate with github"}, err
	}

	return RepoSearchGETResponse{
		Repos: result.Repositories,
		TotalRepos: result.GetTotal(),
	}, requests.ResponseOK, nil
}




