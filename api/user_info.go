package api

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

type UserInfoGETResponse struct {
	Username string `json:"username"`
	Id int `json:"id"`
	Administrator bool `json:"administrator"`
}

// UserInfoGET gets information about the current user
func UserInfoGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	user := r.Context().Value("user").(*jwt.Token)

	jwtMetadata, err := auth.GetJWTMetadata(user)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	userInfo, err := ctx.Database.GetUserById(jwtMetadata.UserId)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	return UserInfoGETResponse{
		Username: userInfo.Username,
		Id: userInfo.UserId,
		Administrator: userInfo.Administrator}, requests.ResponseOK, nil
}

