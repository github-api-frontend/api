package api

import (
	"github.com/google/go-github/github"
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
)

type RepoInfoGETRequestParams struct {
	 Repo int64 `schema:"repo,required" valid:"int,required"`
}

type RepoInfoGETResponse struct {
	Repo *github.Repository `json:"repo"`
}

// RepoInfoGET returns all of the information about the given repo
func RepoInfoGET(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	var requestParams RepoInfoGETRequestParams

	if response, err := decodeAndValidateSchema(r, &requestParams); err != nil {
		return nil, response, err
	}

	repo, _, err := ctx.Github.Repositories.GetByID(ctx.GithubContext, requestParams.Repo)

	if err != nil {
		return nil, &requests.Response{Status: 500, Message: "failed to communicate with github"}, err
	}

	return RepoInfoGETResponse{Repo: repo}, requests.ResponseOK, nil
}