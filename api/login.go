package api

import (
	"gitlab.com/github-api-frontend/api/app/context"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"gitlab.com/github-api-frontend/api/requests"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http"
	"strings"
	"time"
)

type LoginPOSTRequest struct {
	Username string `json:"username" valid:"ascii,length(3|24),required"`
	Password string `json:"password" valid:"ascii,length(8|24),required"`
}

type LoginPOSTResponse struct {
	Token string `json:"token"`
}

// LoginPOST authenticates a user and returns a JWT token
func LoginPOST(ctx *context.Context, w http.ResponseWriter, r *http.Request) (interface{}, *requests.Response, error) {
	var requestData LoginPOSTRequest
	if response, err := parseAndValidateBody(r, &requestData); err != nil {
		return nil, response, err
	}

	requestData.Username = strings.ToLower(requestData.Username)

	validCreds, err := auth.CheckValidUser(ctx, requestData.Username, requestData.Password)
	if !validCreds {
		return nil, &requests.Response{Status: 400, Message: "bad credentials"}, nil
	}

	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	userInfo, err := ctx.Database.GetUser(requestData.Username)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	token, err := auth.CreateJWTToken(userInfo.UserId)
	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	if err := ctx.Database.UserUpdateOrigin(userInfo.UserId, r.RemoteAddr); err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	_, err = ctx.Database.AddEvent(database.Event{
		Timestamp: primitive.NewDateTimeFromTime(time.Now()),
		UserId:    userInfo.UserId,
		IP:        r.RemoteAddr,
		Action:    "LOGIN",
		Details:   "username: " + requestData.Username,
	})

	if err != nil {
		return nil, requests.ResponseInternalServerError, err
	}

	auth.SetResponseJWTCookie(w, token)

	return LoginPOSTResponse{Token: token}, requests.ResponseOK, err
}
