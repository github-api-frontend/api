package tests

import (
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	testFavoriteAddUsername = "test_favorite_add_user"
	testFavoriteAddPassword = "test_password"
)

func TestFavoriteDatabase(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testFavoriteAddUsername,
		Password: testFavoriteAddPassword,
	})

	assert.Nil(t, testContext.Service.Context.Database.AddFavorite(id, 123454))

	favorites, err := testContext.Service.Context.Database.GetFavorites(id)
	assert.Nil(t, err)
	assert.Equal(t, database.Favorite(123454), favorites.Favorites[0])

	err = testContext.Service.Context.Database.RemoveFavorite(id, 123454)
	assert.Nil(t, err)

	favorites, err = testContext.Service.Context.Database.GetFavorites(id)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(favorites.Favorites))
}

