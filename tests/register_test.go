package tests

import (
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	testRegisterUsername = "test_register_username"
	testRegisterPassword = "test_password"
)

func TestRegisterPOST(t *testing.T) {
	// Register a user through the endpoint
	deleteUserByName(t, testRegisterUsername)

	req := makeJSONRequest(t, "POST", "/register", auth.UserInfo{
		Username: testRegisterUsername,
		Password: testRegisterPassword,
	})

	t.Cleanup(func() {
		deleteUserByName(t, testRegisterUsername)
	})

	var registerResult api.RegisterPOSTResponse
	resp, code := getJSONResponse(t, req, &registerResult)
	assert.Equal(t, 200, code)

	testJWTCookie(t, resp)

	assert.True(t, userExistsByName(t, testRegisterUsername))

	userInfo := getUserByName(t, testRegisterUsername)

	metadata, err := auth.GetJWTMetadataFromString(registerResult.Token)
	assert.Nil(t, err)

	assert.Equal(t, metadata.UserId, userInfo.UserId)

	// Check that a user can't be registered twice
	_, code = getJSONResponse(t, req, &registerResult)
	assert.NotEqual(t, 200, code)
}
