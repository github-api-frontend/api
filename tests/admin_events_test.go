package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"net/http"
	"testing"
)

const (
	testAdminEventsUsername = "test_admin_events"
	testAdminEventsPassword = "test_password"
)

func TestAdminEvents(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testAdminEventsUsername,
		Password: testAdminEventsPassword,
	})

	token := createJWTToken(t, id)

	req, err := http.NewRequest("GET", "/admin/events", nil)
	assert.Nil(t, err)

	req.Header.Set("Authorization", "Bearer " + token)

	var response api.AdminEventsGETResponse
	_, code := getJSONResponse(t, req, &response)
	assert.NotEqual(t, 200, code)

	testContext.Service.Context.Database.UserUpdateAdministrator(id, true)
	_, code = getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)

	assert.Less(t, -1, len(response.Events))
}
