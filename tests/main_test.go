package tests

import (
	"gitlab.com/github-api-frontend/api/app"
	"log"
	"os"
	"testing"
)

type testData struct {
	Service *app.Service
}

var testContext testData

func TestMain(m *testing.M) {
	service, err := app.NewService()
	if err != nil {
		log.Fatal(err)
	}

	service.Initialize()

	testContext.Service = service

	code := m.Run()

	os.Exit(code)
}
