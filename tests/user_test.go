package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/github-api-frontend/api/auth"
	"testing"
)

const (
	testUserRegistrationUsername = "test_user_registration"
	testUserRegistrationPassword = "test_password"

	testUserDeletionIDUsername = "test_user_deletion"
	testUserDeletionIDPassword = "test_password"

	testUserUpdateOriginUsername = "test_user_deletion"
	testUserUpdateOriginPassword = "test_password"
)

func TestUserRegistrationAndDeletion(t *testing.T) {
	registerUser(t, auth.UserInfo{
		Username: testUserRegistrationUsername,
		Password: testUserRegistrationPassword,
	})
}

func TestUserDeletionById(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserDeletionIDUsername,
		Password: testUserDeletionIDPassword,
	})

	deleteUser(t, id)
}

func TestUserUpdateOrigin(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserDeletionIDUsername,
		Password: testUserDeletionIDPassword,
	})

	userInfo := getUser(t, id)
	assert.Equal(t, "", userInfo.LoginOrigin)

	testContext.Service.Context.Database.UserUpdateOrigin(id, "12.34.56.78")
	userInfo = getUser(t, id)
	assert.Equal(t, "12.34.56.78", userInfo.LoginOrigin)
}

func TestUserAdministrator(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserDeletionIDUsername,
		Password: testUserDeletionIDPassword,
	})

	userInfo := getUser(t, id)
	assert.Equal(t, false, userInfo.Administrator)

	testContext.Service.Context.Database.UserUpdateAdministrator(id, true)
	userInfo = getUser(t, id)
	assert.Equal(t, true, userInfo.Administrator)
}