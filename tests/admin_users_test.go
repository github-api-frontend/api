package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"net/http"
	"testing"
)

const (
	testAdminUsersUsername = "test_admin_users"
	testAdminUsersPassword = "test_password"
)

func TestAdminUsers(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testAdminEventsUsername,
		Password: testAdminEventsPassword,
	})

	token := createJWTToken(t, id)

	req, err := http.NewRequest("GET", "/admin/users", nil)
	assert.Nil(t, err)

	req.Header.Set("Authorization", "Bearer " + token)

	var response api.AdminUsersGETResponse
	_, code := getJSONResponse(t, req, &response)
	assert.NotEqual(t, 200, code)

	testContext.Service.Context.Database.UserUpdateAdministrator(id, true)
	_, code = getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)

	assert.Less(t, 0, len(response.Users))
}

