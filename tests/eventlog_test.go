package tests

import (
	"gitlab.com/github-api-frontend/api/database"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
	"time"
)

func TestEventLog(t *testing.T) {
	db := testContext.Service.Context.Database

	originalEvent := database.Event{
		Timestamp: primitive.NewDateTimeFromTime(time.Now()),
		UserId:    12345,
		IP:        "12.34.56.78",
		Action:    "Register",
		Details:   "N/A",
	}

	id, err := db.AddEvent(originalEvent)

	t.Cleanup(func() {
		db.RemoveEvent(id)
	})

	assert.Nil(t, err)

	newEvent, err := db.GetEvent(id)
	assert.Nil(t, err)

	assert.EqualValues(t, originalEvent, newEvent)

	events, err := db.GetAllEvents()
	assert.Nil(t, err)

	assert.Less(t, 0, len(events))
}