package tests

import (
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

const (
	testUserInfoUsername = "test_user_info_get"
	testUserInfoPassword = "aaaaaaa"
)

func TestUserInfoGET(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserInfoUsername,
		Password: testUserInfoPassword,
	})
	
	token := createJWTToken(t, id)

	req, err := http.NewRequest("GET", "/user/info", nil)
	assert.Nil(t, err)

	req.Header.Set("Authorization", "Bearer " + token)

	var response api.UserInfoGETResponse

	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, testUserInfoUsername, response.Username)
	assert.Equal(t, id, response.Id)

	req.Header.Set("Authorization", "Bearer 12345" + token)
	_, code = getJSONResponse(t, req, &response)
	assert.NotEqual(t, 200, code)
}

