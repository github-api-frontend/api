package tests

import (
	"gitlab.com/github-api-frontend/api/api"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestRepoInfoGET(t *testing.T) {
	req, err := http.NewRequest("GET", "/repo/6051812/info", nil)
	assert.Nil(t, err)

	var response api.RepoInfoGETResponse

	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, "mux", *response.Repo.Name)
}


