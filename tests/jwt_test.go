package tests

import (
	"gitlab.com/github-api-frontend/api/auth"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestJWTSigning(t *testing.T) {
	token := createJWTToken(t, 12345)

	metadata, err := auth.GetJWTMetadataFromString(token)
	assert.Nil(t, err)

	assert.Equal(t, 12345, metadata.UserId)
}