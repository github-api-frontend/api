package tests

import (
	"bytes"
	"encoding/json"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func getJSONResponse(t *testing.T, request *http.Request, v interface{}) (*http.Response, int) {
	rr := httptest.NewRecorder()
	testContext.Service.Router.Mux().ServeHTTP(rr, request)

	assert.Nil(t, json.Unmarshal(rr.Body.Bytes(), v))

	return rr.Result(), rr.Code
}

func makeJSONRequest(t *testing.T, method string, route string, v interface{}) *http.Request {
	requestBody, err := json.Marshal(v)
	assert.Nil(t, err)

	req, err := http.NewRequest(method, route, bytes.NewBuffer(requestBody))
	assert.Nil(t, err)

	return req
}

func registerUser(t *testing.T, info auth.UserInfo) int {
	id, err := auth.RegisterUser(testContext.Service.Context, info)
	assert.Nil(t, err)

	assert.Greater(t, id, 0)

	assert.True(t, userExists(t, id))

	t.Cleanup(func() {
		deleteUser(t, id)
	})

	return id
}

func deleteUser(t *testing.T, id int) {
	err := testContext.Service.Context.Database.DeleteUserById(id)
	assert.Nil(t, err)

	assert.False(t, userExists(t, id))
}

func deleteUserByName(t *testing.T, name string) {
	err := testContext.Service.Context.Database.DeleteUser(name)
	assert.Nil(t, err)

	assert.False(t, userExistsByName(t, name))
}

func userExistsByName(t *testing.T, name string) bool{
	exists, err := testContext.Service.Context.Database.UserExists(name)
	assert.Nil(t, err)

	return exists
}

func userExists(t *testing.T, id int) bool {
	exists, err := testContext.Service.Context.Database.UserExistsById(id)
	assert.Nil(t, err)

	return exists
}

func getUser(t *testing.T, id int) database.UserInfo {
	userInfo, err := testContext.Service.Context.Database.GetUserById(id)
	assert.Nil(t, err)

	return userInfo
}

func getUserByName(t *testing.T, name string) database.UserInfo {
	userInfo, err := testContext.Service.Context.Database.GetUser(name)
	assert.Nil(t, err)

	return userInfo
}

func createJWTToken(t *testing.T, id int) string {
	token, err := auth.CreateJWTToken(id)
	assert.Nil(t, err)

	return token
}

func testJWTCookie(t *testing.T, r *http.Response) {
	tokenFound := false

	for _, cookie := range r.Cookies() {
		if cookie.Name == "jwt" {
			tokenFound = true
		}
	}

	assert.True(t, tokenFound)
}

func addFavorite(t *testing.T, id int, favorite database.Favorite) {
	assert.Nil(t, testContext.Service.Context.Database.AddFavorite(id, favorite))
}

func getFavorites(t *testing.T, id int) []database.Favorite {
	favorites, err := testContext.Service.Context.Database.GetFavorites(id)
	assert.Nil(t, err)

	return favorites.Favorites
}