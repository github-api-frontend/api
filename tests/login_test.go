package tests

import (
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	testLoginBadUsername = "bad_username"
	testLoginWorkingUsername = "test_login_user"
	testLoginWorkingPassword = "test_login_password"
)

func TestLoginPOST(t *testing.T) {
	// Test an invalid login
	req := makeJSONRequest(t, "POST", "/login", auth.UserInfo{
		Username: testLoginBadUsername,
		Password: testLoginBadUsername,
	})

	var loginResult api.LoginPOSTResponse
	resp, code := getJSONResponse(t, req, &loginResult)
	assert.Equal(t, 400, code)

	// Test a valid login
	id := registerUser(t, auth.UserInfo{
		Username: testLoginWorkingUsername,
		Password: testLoginWorkingPassword,
	})

	req = makeJSONRequest(t, "POST", "/login", auth.UserInfo{
		Username: testLoginWorkingUsername,
		Password: testLoginWorkingPassword,
	})

	resp, code = getJSONResponse(t, req, &loginResult)
	assert.Equal(t, 200, code)

	testJWTCookie(t, resp)

	metadata, err := auth.GetJWTMetadataFromString(loginResult.Token)
	assert.Nil(t, err)

	assert.Equal(t, metadata.UserId, id)
}