package tests

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/github-api-frontend/api/api"
	"gitlab.com/github-api-frontend/api/auth"
	"gitlab.com/github-api-frontend/api/database"
	"gitlab.com/github-api-frontend/api/requests"
	"net/http"
	"testing"
)

const (
	testUserFavoritesUsername = "test_user_favorites"
	testUserFavoritesPassword = "aaaaaaa"
)

func TestUserFavoritesGET(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserFavoritesUsername,
		Password: testUserFavoritesPassword,
	})

	token := createJWTToken(t, id)

	addFavorite(t, id, 1234543)

	req, err := http.NewRequest("GET", "/user/favorites", nil)
	assert.Nil(t, err)

	req.AddCookie(&http.Cookie{
		Name:       "jwt",
		Value:      token,
		HttpOnly:   true,
	})

	var response api.UserFavoritesGETResponse

	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, database.Favorite(1234543), response.Favorites[0])
}

func TestUserFavoritesIDGET(t *testing.T) {
	id := registerUser(t, auth.UserInfo{
		Username: testUserFavoritesUsername,
		Password: testUserFavoritesPassword,
	})

	token := createJWTToken(t, id)

	addFavorite(t, id, 1234543)

	// Try valid repo
	req, err := http.NewRequest("GET", "/user/favorite/1234543", nil)
	assert.Nil(t, err)

	req.Header.Set("Authorization", "Bearer " + token)

	var response api.UserFavoriteGETResponse

	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, true, response.Favorited)

	// Try invalid repo
	req, err = http.NewRequest("GET", "/user/favorite/12345433", nil)
	assert.Nil(t, err)

	req.Header.Set("Authorization", "Bearer " + token)

	_, code = getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, false, response.Favorited)
}

func TestUserFavoritesPUT(t *testing.T) {
	deleteUserByName(t, testUserFavoritesUsername)

	id := registerUser(t, auth.UserInfo{
		Username: testUserFavoritesUsername,
		Password: testUserFavoritesPassword,
	})

	token := createJWTToken(t, id)

	req := makeJSONRequest(t, "PUT", "/user/favorites",
		api.UserFavoritesPUTRequest{Favorite: 12345})

	req.Header.Set("Authorization", "Bearer " + token)

	var response requests.Response

	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, database.Favorite(12345), getFavorites(t, id)[0])

	_, code = getJSONResponse(t, req, &response)
	assert.NotEqual(t, 200, code)
}

func TestUserFavoritesDELETE(t *testing.T) {
	deleteUserByName(t, testUserFavoritesUsername)

	id := registerUser(t, auth.UserInfo{
		Username: testUserFavoritesUsername,
		Password: testUserFavoritesPassword,
	})

	addFavorite(t, id, 12345)

	req := makeJSONRequest(t, "DELETE", "/user/favorites",
		api.UserFavoritesDELETERequest{Favorite: 12345})

	token := createJWTToken(t, id)
	req.Header.Set("Authorization", "Bearer " + token)

	assert.Equal(t, 1, len(getFavorites(t, id)))

	var response requests.Response
	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)
	assert.Equal(t, 0, len(getFavorites(t, id)))
}
