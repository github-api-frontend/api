package tests

import (
	"gitlab.com/github-api-frontend/api/api"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRepoSearchGET(t *testing.T) {
	req := makeJSONRequest(t, "POST", "/repo/search", api.RepoSearchPOSTRequest{
		Query:   "test",
		Page:    0,
		PerPage: 5,
	})

	var response api.RepoSearchGETResponse
	_, code := getJSONResponse(t, req, &response)
	assert.Equal(t, 200, code)

	assert.Equal(t, 5, len(response.Repos))
}