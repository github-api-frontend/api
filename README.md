# Github API Frontend API
This repository stores the code for the primary back-end REST API service.

## Deployment
This application can be compiled and tested through the provided Makefile or through the Docker configuration.
This service relies on MongoDB and Redis and will not function properly without them.

### Using Make
``make build && ./bin/github-api-frontend``

### Using Docker
``docker run -it -p 80:80 registry.gitlab.com/github-api-frontend/api:latest``

### Using Docker-Compose (includes MongoDB and Redis)
``docker-compose up``

## Configuration
This application is configured entirely through environment variables which are defined in `gaf.env`.

## REST API Endpoints
All successful actions are represented by an OK response code (200).

### Unauthorized Routes
| Route             | Method | JSON Request                                | Successful JSON Response            |
|-------------------|--------|---------------------------------------------|-------------------------------------|
| /login            | POST   | { username: String, password: String }      | { token: String }                     |
| /register         | POST   | { username: String, password: String }      | { token: String }                     |
| /repo/search      | POST   | { query: String, page: int, per_page: int } | { total: int, Repos: []Repository } |
| /repo/{repo}/info | GET    | {}                                          | { repo: Repository }                |

### User-Authorized Routes
| Route                   | Method | JSON Request      | Successful JSON Response                           |
|-------------------------|--------|-------------------|----------------------------------------------------|
| /user/info              | GET    | {}                | { username: String, id: int, administrator: bool } |
| /user/favorites         | GET    | {}                | { favorites: []int }                                 |
| /user/favorite/{repoId} | GET    | {}                | { favorited: bool }                                  |
| /user/favorites         | PUT    | { favorite: int } | {}                                                 |
| /user/favorites         | DELETE | { favorite: int } | {}                                                 |

### Administrator-Authorized Routes
| Route         | Method | JSON Request | Successful JSON Response |
|---------------|--------|--------------|--------------------------|
| /admin/events | GET    | {}           | { events: []Event }        |
| /admin/users  | GET    | {}           | { users: []User }          |

## Custom JSON Types
### Repository
- Defined by the go-github package

### User
| Field         | Type   |
|---------------|--------|
| userid        | int    |
| username      | String |
| login_origin  | string |
| administrator | bool   |

### Event
| Field     | Type     |
|-----------|----------|
| timestamp | DateTime |
| userid    | int      |
| ip        | String   |
| action    | String   |
| details   | String   |
