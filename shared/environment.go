package shared

import (
	"os"
	"strings"
)

var (
	EnvUseHTTPS = os.Getenv("GAF_USE_HTTPS")
	EnvCertFile = os.Getenv("GAF_CERT")
	EnvKeyFile = os.Getenv("GAF_KEY")
	EnvOriginPolicy = strings.Split(os.Getenv("GAF_CORS_ORIGINS"), ",")
	EnvAPIHostAddress = os.Getenv("GAF_API_HOST")
	EnvRedisHost = os.Getenv("GAF_REDIS_HOST")
	EnvMongoConnection = os.Getenv("GAF_MONGO_CONNECTION")
	EnvJWTSigningKey = os.Getenv("GAF_JWT_SIGNING_KEY")
	EnvGithubToken = os.Getenv("GAF_GITHUB_TOKEN")
)
